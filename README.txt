Description
-----------
This module enables communication with AFAS, an ERP / CRM system (www.afas.nl).
The module contains some configuration wrappers / screens and a screen to test
communication, around the PracticalAfas PHP library (written by the module
author).


Installing / Configuration
--------------------------

1) Install the PracticalAfas PHP library, and make sure it can be autoloaded.

Drupal 7 doesn't really have a unified way of installing libraries, nowadays. A
few options for installing are:

a) Use the composer_manager module to install the library. (This module ships
   with a composer.json file.)

b) Use your own composer workflow. (Some Drupal developers prefer to e.g. keep
   all dependencies in their own root composer.json instead of option a.)

c) If you don't want to use composer: download the library yourself, from
   https://github.com/rmuit/PracticalAfas . The most obvious place to put it is
   sites/all/libraries/practicalafas. This means you must take care that the
   library classes are loaded before calling any of the afas_api functions or
   visiting admin/config/services/afas. One convenient way of doing this is:

   - To any .info file of an enabled module (e.g. one of your custom modules),
     add the following line:

    registry_autoload[DRUPAL_ROOT/sites/all/libraries/practicalafas/src] = PSR-4

     (Keep the literal "DRUPAL_ROOT" in there, and adjust the directory if
     necessary.)

   - Then install the registry_autoload module. After installing, the library's
     class files should be loaded automatically on demand. No further
     configuration is necessary.

d) Download the library yourself, and use your own autoloader or 'manually'
   include the files before using them / calling any afas_api functions.


2) Install / enable the module as one normally would.

If you plan to use only one set of credentials and want to configure these in
the Drupal site, visit admin/config/services/afas and configure the Customer ID
and App Token there. (If this breaks, it is likely that autoloading the
PracticalAfas classes is failing.) After this, various options on the 'Test' tab
can be used to test fetching some data.


Advanced configuration / coding
-------------------------------
If this module was not installed because another module needs it, this implies
you want to experiment / code things yourself. You can start exploring in one of
two directions:

- Read the README.md from the library.

- Read the two helper functions in afas_api.module which wrap around the
  library and do the configuration (from the admin screen) and Drupal logging
  for you. These are afas_api_get_data() and afas_api_send_data(). They
  hopefully speak for themselves.

If you don't need Drupal's standard admin screen configuration or the error
handling which is provided by afas_api_get/send_data(), then it is quite
possible you do not need this module at all. It can be useful to make use of the
admin screens for testing the connection, but you may consider shipping your
'end product' site without this module and only including the PracticalAfas
library. (The examples in its README.md show that there is not much to it. You
just need to catch your own exceptions and do logging.)


Support
-------
Filing bugs or patches for this module can be done at
http://drupal.org/project/issues/afas_api. Filing bugs or PRs for the included
library can be done at the libary's github repository
https://github.com/rmuit/PracticalAfas

The author welcomes any feedback, suggestions and especially contributions. The
author also welcomes any paid work with AFAS or related integrations; open
source is provided out of free will but does not have to be gratis. All other
suggestions / bug reports are appreciated and will be handled as free time
allows.
