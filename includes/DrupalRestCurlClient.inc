<?php

use PracticalAfas\RestCurlClient;

/**
 * Subclass of \PracticalAfas\SoapAppClient, handling Drupal configuration.
 */
class DrupalRestCurlClient extends RestCurlClient {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $options = array()) {
    // If options are not provided (which in practice will be always), fill them
    // from our global Drupal configuration variables.
    foreach (array(
               'customerId' => array('afas_api_customer_id', ''),
               'appToken' => array('afas_api_app_token', ''),
             ) as $required_key => $var) {
      if (!isset($options[$required_key])) {
        $options[$required_key] = variable_get($var[0], $var[1]);
      }
      // We know the parent will throw an exception. We'll set a more specific
      // message.
      if (empty($options[$required_key])) {
        $classname = get_class($this);
        throw new \InvalidArgumentException("Required configuration parameter for $classname missing: $required_key. Maybe you forgot to set the module configuration?", 1);
      }
    }

    parent::__construct($options);
  }

  /**
   * Adds class specific options to the settings form.
   *
   * @return array
   *   Extra form elements to merge into the settings form.
   */
  public static function settingsForm() {

    $form['afas_api_customer_id'] = array(
      '#type' => 'textfield',
      '#title' => t('Customer / User ID'),
      '#description' => t('This is usually 5 digits.'),
      '#default_value' => variable_get('afas_api_customer_id', ''),
      '#size' => 20,
    );
    $form['afas_api_app_token'] = array(
      '#type' => 'textfield',
      '#title' => t('The token for the app connector'),
      '#description' => t('A note: if it expires, this module has no automatic way yet of creating a new one.'),
      '#default_value' => variable_get('afas_api_app_token', ''),
    );

    return $form;
  }

}
