<?php
/**
 * @file
 * Admin screens for AFAS API.
 */

use PracticalAfas\Connection;

/**
 * Form definition for global settings.
 */
function afas_api_settings_form($form, &$form_state) {

  $form['logging'] = array(
    '#type' => 'fieldset',
    '#title' => t('Logging'),
    '#description' => t('These logging settings are followed by code which uses the standard functions in afas_api.module.'),
  );
  // These are various combinations of the 'watchdog' + 'watchdog_extra' options
  // for the logger class.
  $form['logging']['afas_api_log_watchdog'] = array(
    '#type' => 'select',
    '#title' => t('Log AFAS errors to watchdog'),
    '#options' => array(
      0 => t('No'),
      AFAS_LOG_ERRORS_WATCHDOG => t('Log errors'),
      AFAS_LOG_ERRORS_WATCHDOG+AFAS_LOG_ERROR_DETAILS_WATCHDOG => t('Log all messages including all context info + backtrace'),
    ),
    '#default_value' => variable_get('afas_api_log_watchdog', 1),
  );
  $form['logging']['afas_api_log_screen'] = array(
    '#type' => 'select',
    '#title' => t('Show AFAS errors on screen'),
    '#options' => array(
      0 => t('No'),
      AFAS_LOG_ERRORS_SCREEN => t('Log errors'),
      AFAS_LOG_ERRORS_SCREEN+AFAS_LOG_ERROR_DETAILS_SCREEN => t('Log all messages including all context info + backtrace'),
    ),
    '#default_value' => variable_get('afas_api_log_screen', 0),
  );

  $form['class'] = array(
    '#type' => 'fieldset',
    '#title' => t('Client class'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  // Get class name from submitted AJAX form on reload, so the right form gets
  // displayed.
  if (!empty($form_state['values']['afas_api_client_class'])) {
    $current_classname = $form_state['values']['afas_api_client_class'];
  }
  else {
    $current_classname = variable_get('afas_api_client_class', 'DrupalSoapAppClient');
  }
  $info = module_invoke_all('afas_api_client_info');
  $form['class']['afas_api_client_class'] = array(
    '#type' => 'select',
    '#title' => t('The name of the AFAS soap client class to use'),
    '#options' => array_combine(array_keys($info), array_keys($info)),
    '#default_value' => $current_classname,
    '#ajax' => array(
      'callback' => 'afas_api_admin_class_options_callback',
      'wrapper' => 'class-specific',
      'method' => 'replace',
    ),
  );
  $form['class']['info'] = array(
    '#markup' => '<p>' . t('More information:') . '</p><p>',
    '#prefix' => '<div class="description">',
    '#suffix' => '</div>',
  );
  foreach ($info as $classname => $description) {
    $form['class']['info']['#markup'] .= '<li>' . $classname . ': '
                                         . $description . '</li>';
  }
  $form['class']['info']['#markup'] .= '</p>';

  $form['class_specific'] = array(
    '#type' => 'fieldset',
    '#prefix' => '<div id="class-specific">',
    '#suffix' => '</div>',
    '#title' => t('Client specific options'),
    '#collapsible' => FALSE,
    '#description' => t("Changing an option for one class may have an effect on other classes' configuration too."),
  );
  if (method_exists($current_classname, 'settingsForm')) {
    $form['class_specific'] += $current_classname::settingsForm();
  }

  $form['#validate'][] = 'afas_api_settings_form_validate';

  return system_settings_form($form);
}

/**
 * AJAX form callback for the settings form.
 */
function afas_api_admin_class_options_callback($form, $form_state) {
  return $form['class_specific'];
}

/**
 * Validate function for our global settings form.
 */
function afas_api_settings_form_validate($form, &$form_state) {
  if (!empty($form_state['values']['afas_api_client_class'])) {
    $current_classname = $form_state['values']['afas_api_client_class'];
  }
  else {
    $current_classname = variable_get('afas_api_client_class', 'DrupalSoapAppClient');
  }
  if (method_exists($current_classname, 'settingsFormValidate')) {
    $current_classname::settingsFormValidate($form, $form_state);
  }
}

/**
 * Return the client type of the currently configured default client.
 *
 * @return string
 *   'SOAP' or 'REST'.
 */
function afas_api_test_client_type() {
  $current_classname = variable_get('afas_api_client_class', 'DrupalSoapAppClient');
  $client = new $current_classname();
  $class = get_class($client);
  // Set the type so the rest of our code can decide on logic to follow.
  // Historically, SOAP classes have not had a type defined.
  return defined("$class::CLIENT_TYPE") ? $class::CLIENT_TYPE : 'SOAP';
}

/**
 * Form definition for tests form.
 */
function afas_api_test_form($form, &$form_state) {
  $type_soap = afas_api_test_client_type() === 'SOAP';

  $form['help'] = array(
    '#markup' => t('On this form you can perform certain remote calls, which eases testing API access and accessing certain data. The options are slightly different depending on the type of selected client class (SOAP or REST).'),
    '#weight' => 0,
  );

  $form['getconnector'] = array(
    '#type' => 'fieldset',
    '#title' => t('Get data from a GetConnector'),
  );
  $form['getconnector']['getconnector_name'] = array(
    '#type' => 'textfield',
    '#title' => t('GetConnector name'),
  );
  $form['getconnector']['getconnector_skip'] = array(
    '#type' => 'textfield',
    '#title' => t('Skip # of items before reading data'),
  );
  $form['getconnector']['getconnector_take'] = array(
    '#type' => 'textfield',
    // Adjust title to the specific client's behavior.
    '#title' => t('Read/return # of items') . ' ' . ($type_soap ? t('(cannot be 0)') : t('(default: 100)')),
  );
  $form['getconnector']['getconnector_orderby'] = array(
    '#type' => 'textfield',
    '#title' => t('Order by fields'),
    '#description' => t('Syntax example: Field1,-Field2 for ascending and then descending ordering.'),
  );
  $form['getconnector']['getconnector_filter'] = array(
    '#type' => 'textarea',
    '#rows' => 5,
    '#title' => t('Filter'),
    '#description'   => "Fill one or more (comma separated) array definitions to apply filters. (This format is a slightly simplified version of the argument to Connection::getData().)<br>    
One example, to get only items that changed in the past week:<br><em>array(<br>
'A-DATE-FIELD' => '" . date('Y-m-d\TH:i:s', time() - 7 * 86400) . "',<br>
'#op' => \\PracticalAfas\\Connection::OP_LARGER_OR_EQUAL,<br>
),</em>",
    '#default_value' => "array(
),",
  );
  $form['getconnector']['getconnector_get_metadata'] = array(
    '#type' => 'checkbox',
    '#title' => t('Include metadata'),
  );
  if (!$type_soap) {
    $form['getconnector']['getconnector_get_metadata']['#states']['enabled'] = array(
      ':input[name="getconnector_get_literal"]' => array('checked' => FALSE),
    );
  }
  else {
    // 'Metadata' only makes sense for XML output; if we're converting to an
    // array, the Metdadata == Schema information gets swallowed.
    $form['getconnector']['getconnector_get_metadata']['#states']['disabled'] = array(
      ':input[name="getconnector_get_literal"]' => array('checked' => FALSE),
      ':input[name="getconnector_output_pretty"]' => array('checked' => FALSE)
    );
    $form['getconnector']['getconnector_get_empty'] = array(
      '#type' => 'checkbox',
      '#title' => t('Include empty field values'),
    );
  }
  $form['getconnector']['getconnector_get_literal'] = array(
    '#type' => 'checkbox',
    '#title' => t('Output literal return value from remote service'),
    '#states' => array('enabled' => array(':input[name="getconnector_output_pretty"]' => array('checked' => FALSE))),
  );
  if ($type_soap) {
    $form['getconnector']['getconnector_output_pretty'] = array(
      '#type' => 'checkbox',
      '#title' => t('Output pretty printed XML.'),
    );
  }
  $form['getconnector']['getconnector_get'] = array(
    '#type' => 'submit',
    '#value' => t('Get Data'),
  );

  if ($type_soap) {
    $form['get_schema'] = array(
      '#type' => 'fieldset',
      '#title' => t('Get XSD schema for UpdateConnector'),
    );
    $form['get_schema']['get_schema_function'] = array(
      '#type' => 'textfield',
      '#title' => 'UpdateConnector name',
    );
    $form['get_schema']['get_schema_submit'] = array(
      '#type' => 'submit',
      '#value' => t('Get XSD'),
    );
  }
  else {
    $form['get_schema'] = array(
      '#type' => 'fieldset',
      '#title' => t('Get meta info'),
    );
    $form['get_schema']['get_schema_get'] = array(
      '#type' => 'textfield',
      '#title' => 'GetConnector name',
    );
    $form['get_schema']['get_schema_update'] = array(
      '#type' => 'textfield',
      '#title' => 'UpdateConnector name',
    );
    $form['get_schema']['get_schema_submit'] = array(
      '#type' => 'submit',
      '#value' => t('Get Meta Info'),
    );
  }

  // Token isn't implemented for REST yet in the library but since we haven't
  // even seen it work for SOAP yet, we'll just keep it visible for now and have
  // it throw an exception.
  $form['get_token'] = array(
    '#type' => 'fieldset',
    '#title' => t('Get new token'),
    '#weight' => 11,
  );
  $form['get_token']['get_token_user_id'] = array(
    '#type' => 'textfield',
    '#title' => t('User ID'),
  );
  $form['get_token']['get_token_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('API key'),
  );
  $form['get_token']['get_token_environment_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Environment key'),
  );
  $form['get_token']['get_token_description'] = array(
    '#type' => 'textfield',
    '#title' => t('Description'),
  );
  $form['get_token']['get_token_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Get Token'),
  );


  $form['get_version'] = array(
    '#type' => 'fieldset',
    '#title' => t('Get version info'),
    '#weight' => 11,
  );
  $form['get_version']['get_version_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Get Version'),
  );

  return $form;
}

/**
 * Form valiate callback for tests form.
 */
function afas_api_test_form_validate(&$form, &$form_state) {
  if (!empty($form_state['values']['get_schema_get']) && !empty($form_state['values']['get_schema_update'])) {
    form_error($form['get_schema']['get_schema_update'], t('Enter either a GetConnector or an UpdateConnector, not both.'));
  }
  if (!empty($form_state['values']['getconnector_filter'])) {
    $eval = '';
    eval('$eval = array(' . $form_state['values']['getconnector_filter'] . ');');
    if (is_array($eval)) {
      foreach ($eval as $filter) {
        if (!is_array($filter)) {
          form_error($form['getconnector']['getconnector_filter'], t("Invalid 'filter' (part): %p", ['%p' => var_export($filter, TRUE)]));
        }
      }
    }
    else {
      form_error($form['getconnector']['getconnector_filter'], t("Invalid filter: %p", ['%p' => var_export($form_state['values']['getconnector_filter'], TRUE)]));
    }
  }
}

/**
 * Form submit callback for tests form.
 */
function afas_api_test_form_submit(&$form, &$form_state) {
  $type_soap = afas_api_test_client_type() === 'SOAP';
  $result =  FALSE;
  switch ($form_state['clicked_button']['#value']) {
    case t('Get Data'):
      $args = array();
      if (isset($form_state['values']['getconnector_skip']) && $form_state['values']['getconnector_skip'] !== '') {
        $args['skip'] = $form_state['values']['getconnector_skip'];
      }
      if (isset($form_state['values']['getconnector_take']) && $form_state['values']['getconnector_take'] !== '') {
        $args['take'] = $form_state['values']['getconnector_take'];
      }
      if (isset($form_state['values']['getconnector_orderby']) && $form_state['values']['getconnector_orderby'] !== '') {
        // Supported for SOAP as well as REST; the library converts it for SOAP.
        $args['OrderbyFieldIds'] = $form_state['values']['getconnector_orderby'];
      }
      if (!empty($form_state['values']['getconnector_get_literal']) || !empty($form_state['values']['getconnector_output_pretty'])) {
        $args['options']['Outputmode'] = Connection::GET_OUTPUTMODE_LITERAL;
      }
      if (!empty($form_state['values']['getconnector_get_metadata'])) {
        $args['options']['Metadata'] = Connection::GET_METADATA_YES;
      }
      if (!empty($form_state['values']['getconnector_get_empty'])) {
        $args['options']['Outputoptions'] = Connection::GET_OUTPUTOPTIONS_XML_INCLUDE_EMPTY;
      }
      $filters = array();
      if (!empty($form_state['values']['getconnector_filter'])) {
        $eval = '';
        // Note - can cause uncatchable (at least in PHP5?) fatal error...
        eval('$eval = array(' . $form_state['values']['getconnector_filter'] . ');');
        if (is_array($eval)) {
          foreach ($eval as $filter) {
            if (is_array($filter)) {
              if (!empty($filter)) {
                $filters[] = $filter;
              }
              // Just disregard filter-parts which are empty arrays.
            }
            else {
              // This is 'impossible' because we just validated the expression.
              throw new \Exception(t("Invalid 'filter' (part): %p", ['%p' => var_export($filter, TRUE)]));
            }
          }
        }
        else {
          throw new \Exception(t("Invalid filter: %p", ['%p' => var_export($form_state['values']['getconnector_filter'], TRUE)]));
        }
      }

      $result = afas_api_get_data($form_state['values']['getconnector_name'], $filters, Connection::DATA_TYPE_GET, $args, AFAS_LOG_ERROR_DETAILS_SCREEN);
      break;

    case t('Get Version'):
      $args = array();
      if (!$type_soap) {
        $args['options']['Outputmode'] = Connection::GET_OUTPUTMODE_LITERAL;
      }
      $result = afas_api_get_data('', array(), Connection::DATA_TYPE_VERSION_INFO, $args, AFAS_LOG_ERROR_DETAILS_SCREEN);
      break;

    case t('Get Token'):
      $extra_args = array(
        'apiKey' => $form_state['values']['get_token_api_key'],
        'environmentKey' => $form_state['values']['get_token_environment_key'],
        'description' => $form_state['values']['get_token_description'],
      );
      $result = afas_api_get_data($form_state['values']['get_token_user_id'], array(), 'token', $extra_args, AFAS_LOG_ERROR_DETAILS_SCREEN);
      break;

    case t('Get Meta Info'):
      // Empty get connector == empty update connector; there is no difference.
      $args['options']['Outputmode'] = Connection::GET_OUTPUTMODE_LITERAL;
      if (!empty($form_state['values']['get_schema_get'])) {
        $result = afas_api_get_data($form_state['values']['get_schema_get'], array(), 'metainfo_get', $args, AFAS_LOG_ERROR_DETAILS_SCREEN);
      }
      else {
        $data_id = !empty($form_state['values']['get_schema_update']) ? $form_state['values']['get_schema_update'] : '';
        $result = afas_api_get_data($data_id, array(), 'data', $args, AFAS_LOG_ERROR_DETAILS_SCREEN);
      }
      break;

    case t('Get XSD'):
      // Retrieve / display XSD;
      $result = afas_api_get_data($form_state['values']['get_schema_function'], array(), 'data', array(), AFAS_LOG_ERROR_DETAILS_SCREEN);
      if ($result) {

    /* We got an XML string returned, with the XSD encoded into one of the
     * attributes. This is an example string we got back:
<AfasDataConnector>
  <xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:element name="AfasDataConnector">
      <xs:complexType>
        <xs:choice maxOccurs="unbounded">
          <xs:element name="ConnectorData">
            <xs:complexType>
              <xs:sequence>
                <xs:element name="Id" type="xs:string" minOccurs="0"/>
                <xs:element name="Schema" type="xs:string" minOccurs="0"/>
              </xs:sequence>
            </xs:complexType>
          </xs:element>
        </xs:choice>
      </xs:complexType>
    </xs:element>
  </xs:schema>
  <ConnectorData>
    <Id>knOrganisation</Id>
    <Schema> [ THE SCHEMA DATA ] </Schema>
  </ConnectorData>
</AfasDataConnector>
     * Let's decode that. If we find the schema, we will only print that and
     * silently ignore the rest.
     */
      $doc_element = new SimpleXMLElement($result);
      if (isset($doc_element->ConnectorData->Schema)) {
        $result = strval($doc_element->ConnectorData->Schema);
      }
    }
    break;
  }

  if ($result || is_array($result)) {
    // Do not redirect; print the value directly into the form.
    $form_state['redirect'] = FALSE;
    $form['result'] = array(
      '#type' => 'fieldset',
      '#title' => t('RESULTS:'),
      '#weight' => -99,
    );
    if (!empty($form_state['values']['getconnector_output_pretty']) && afas_api_test_client_type() === 'SOAP') {
      // To pretty print it, we need to make a document from it.
      $doc_element = new DomDocument('1,0');
      if ($doc_element->loadXML($result)) {
        $doc_element->formatOutput = TRUE;
        $result = $doc_element->saveXML();
        // The original did not contain the XML header though.
        $len = strlen("<?xml version=\"1.0\"?>\n");
        if (substr($result, 0, $len) === "<?xml version=\"1.0\"?>\n") {
          $result = substr($result, $len);
        }
      }
    }
    if (!is_scalar($result)) {
      // In case of a REST client, we got JSON that was just decoded...
      $result = json_encode($result, JSON_PRETTY_PRINT);
    }
    // NOTE - the literal value from the REST API already contains CRLFs,
    // unlike the SOAP API.

    // Since there will be no rebuild (we will just exit the form builder after
    // this submit callback, and the renderer will work on it), use #value.
    $form['result']['output'] = array(
      '#type'          => 'textarea',
      '#rows'          => 20,
      '#weight' => -99,
      '#value' => $result,
    );
  }
}
