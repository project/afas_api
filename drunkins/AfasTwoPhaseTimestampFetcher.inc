<?php

use PracticalAfas\Connection;

/**
 * Two-phase fetcher for use by jobs fetching 'latest changes' for linked items.
 *
 * With 'linked items' we mean items which actually have two 'updated dates'.
 * As an example: contacts/persons with linked  organization fields, which we
 * also want to re-fetch when only organization-related fields change:
 * - a knContact's 'updated' date changes when the knContact or related KnPerson
 *   object is saved
 * - but if only the related knOrganisation changes, the knContact does not,
 * so if we want to fetch changes to a contact person also if their company name
 * changes, we need to use a getConnector that includes the company's 'updated'
 * date, and configure that as a setting in this job.
 *
 * (I thought I'd tested that the contact's 'updated' date always changed when
 * when we change the organisation - so this might be true for the UI, but it
 * turns out not to be true for e.g. bulk updates Maybe there are also
 * theoretical cases where the knPerson's 'updated' date is more recent than the
 * knContact's, and we should do a 3rd fetch. We have however not seen this yet
 * so we won't do a fetch all the time that is not necessary at all.)
 *
 * About supported options:
 * - afas_skip cannot be reliably supported for the second phase; if the first
 *   phase yields 0 records, we don't know how many to skip in the second.
 * - fetcher_limit is supported but it's a strange option in this context,
 *   because the ordering of items is... non-trivial.

 */
class AfasTwoPhaseTimestampFetcher extends AfasGetConnector {
  /* Settings in $this->settings, used by this class:
   * - 'afas_updated_field_phase_two'
   */

  /**
   * @var string
   */
  protected $originalUpdatedField;

  /**
   * @var int
   */
  protected $originalLimit;

  /**
   * {@inheritdoc}
   */
  public function settingsForm() {
    $form = parent::settingsForm();

    if (isset($form['selection']['afas_skip'])) {
      unset($form['selection']['afas_skip']);
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function fetchItems(array &$context) {
    if (empty($this->settings['afas_updated_field_phase_two'])) {
      throw new Exception(t('@setting setting not configured.', array('@setting' => 'afas_updated_field_phase_two')));
    }
    if (!empty($this->settings['afas_skip'])) {
      throw new Exception(t('@setting setting not supported by @class.', array('@setting' => 'afas_skip', '@class' => get_class($this))));
    }
    // If a previous call to parent::fetchItems() threw an exception, we might
    // be in a strange state with $this->settings not reflecting reality.
    if (isset($this->originalUpdatedField)) {
      throw new Exception(t('@class internal state is confusing. Refusing to do further fetching.', array('@class' => get_class($this))));
    }

    // Adjust values if we're in phase 2.
    if (!empty($context['afas_timestamp_fetcher_phase_two'])) {
      // Switch 'afas_updated_field' setting so the parent will filter (and
      // possibly sort) as we want it to.
      $this->originalUpdatedField = $this->settings['afas_updated_field'];
      $this->settings['afas_updated_field'] = $this->settings['afas_updated_field_phase_two'];
      // Same with limiting items.
      if (isset($this->settings['fetcher_limit'])) {
        $this->originalLimit = $this->settings['fetcher_limit'];
        unset($this->settings['fetcher_limit']);
      }
      // For determining whether to actually set the limit, we'll check only the
      // context value, not the original setting: we assume that if the context
      // value has been tampered with, that's on purpose.
      if (!empty($context['afas_timestamp_fetcher_phase_two_limit'])) {
        $this->settings['fetcher_limit'] = $context['afas_timestamp_fetcher_phase_two_limit'];
      }
    }

    $items = parent::fetchItems($context);

    if ($this->originalUpdatedField) {
      // Switch back the properties, so we won't get confused when this object
      // is used twice for calling start().
      $this->settings['afas_updated_field'] = $this->originalUpdatedField;
      $this->originalUpdatedField = NULL;
      if (isset($this->originalLimit)) {
        $this->settings['fetcher_limit'] = $this->originalLimit;
        $this->originalLimit = NULL;
      }
    }

    // If the process is in the first phase, and finishing that phase, and this
    // job run has a rolling timestamp filter... then set context to 'phase two'
    // (for which we will fetch records when we are called again). Unless we
    // have already fetched enough records according to 'fetcher_limit'.
    if (empty($context['afas_timestamp_fetcher_phase_two'])
        && empty($context['drunkins_process_more'])
        // Check only the context for a date; that's what the parent does too.
        && !empty($context['fetcher_timestamp_from'])
        && empty($this->settings['fetcher_single_item'])
        && (empty($this->settings['fetcher_limit'])
            // The limit can either be equal to the current number of records
            // (if not using batched fetches), or to the counter used by
            // Helper::getDataBatch().
            || $this->settings['fetcher_limit'] > (!empty($context['subtotal']) ? $context['subtotal'] : count($items)))
        ) {
      $context['afas_timestamp_fetcher_phase_two'] = TRUE;
      $context['drunkins_process_more'] = TRUE;
      // Record a new fetcher_limit.
      if (empty($this->settings['fetcher_limit'])) {
        unset($context['afas_timestamp_fetcher_phase_two_limit']);
      }
      else {
        $context['afas_timestamp_fetcher_phase_two_limit'] =
          $this->settings['fetcher_limit'] - (!empty($context['subtotal']) ? $context['subtotal'] : count($items));
      }
      // Reset 'subtotal' to indicate that we're fetching a new data set;
      // otherwise Helper::getDataBatch() adds a bogus filter.
      $context['subtotal'] = 0;

      // @todo we could implement a setting that does a fetchItems() immediately
      //       for 2nd phase, for jobs which know they will never time out. (And
      //       know that their first phase will never return
      //       'drunkins_process_more'; otherwise it would be a bit pointless.)
      //       In this case, at least 'drunkins_rerun_start' context needs to be
      //       set too.
    }

    return $items;
  }

  /**
   * {@inheritdoc}
   */
  public function inputFilter(array $context) {
    $filter = parent::inputFilter($context);

    if (!empty($context['afas_timestamp_fetcher_phase_two'])) {
      // We're filtering on the 'other' updated field now. Add an extra filter
      // to exclude all records we fetched in phase one.
      $filter[] = array(
        $this->originalUpdatedField => date('Y-m-d\TH:i:s', $context['fetcher_timestamp_from']),
        '#op' => Connection::OP_SMALLER_THAN,
      );
    }

    return $filter;
  }

}
