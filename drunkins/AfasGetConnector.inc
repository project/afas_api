<?php

use PracticalAfas\Connection;

/**
 * Drunkins fetcher which retrieves data through an AFAS GetConnector.
 *
 * Other modules can create jobs and make them available to cron / admin UI, by
 * - installing the 'drunkins' Drupal contrib module;
 * - exposing a job definition through hook_queue_info(), which configures this
 *   class as a fetcher. (Or which configures a child class as a job.)
 * See https://www.drupal.org/project/drunkins for more context.
 *
 * If the settings to / context for this class are such that data is fetched in
 * batches, the following keys are overwritten (in Helper::getDataBatch()):
 * subtotal, next_start, last_records, finished. If 'subtotal' is reset between
 * calls to fetchItem(), this means a new multi-batch data set will be fetched.
 * (Never reset 'subtotal' unless 'finished' is true / 'drunkins_process_more'
 * is false.)
 */
class AfasGetConnector extends DrunkinsFetcher {
  /* Settings in $this->settings, used by this class:
   * - job_id:                defined in parent.
   * - opt_fetcher_timestamp: defined in parent.
   * - pkey:                  defined in parent;
   *     used as 'afas_sortable_id_field' if that is not defined but multi-batch
   *     fetching is still enabled (by way of 'fetcher_timestamp_from' context).
   *     (Because we prefer a key to a date field; see below.)
   * - fetcher_limit:         defined in parent
   *     A NOTE - since AFAS GetConnectors cap results (i.e. their 'take'
   *     argument is required), you must make sure either to set a fetcher_limit
   *     or that your job is able to fetch items in multiple batches. (Basically
   *     by setting afas_sortable_id_field.)
   *
   * - afas_take_per_batch (integer; default static::TAKE_DEFAULT):
   *   How many items to fetch in one batch, if 'fetcher_limit' is not set or
   *   is larger than this value. 'fetcher_limit' still ultimately decides how
   *   many items are fetched; this is not a replacement for it. Since this has
   *   a default: if 'fetcher_limit' is set and you want to make sure items are
   *   fetched in one batch (or your job cannot fetch multiple batches),
   *   'afas_take_per_batch' must have an equal/larger value.
   *
   * - afas_api_client_class (string):
   *   The name of the client class to use. Optional; by default the one from
   *   Drupal's global AFAS API admin screen will be taken.
   * - afas_client_options (array):
   *   Configuration options for the client. At least authentication related
   *   options should be provided here, unless the class' constructor knows
   *   where to find its own configuration. (Which is the case for standard
   *   Drupal-admin configured client classes.)
   *
   * - afas_connector (string):
   *   The name of the GetConnector to use for fetching data.
   *
   * - afas_filter (array):
   *   Filter to use. See comments at \PracticalAfas\Connection::parseFilters().
   * - afas_full_data_set_filter (array):
   *   Unlike 'afas_filter', this can only contain a 'single layer' filter - and
   *   unlike 'afas_filter', setting this does not make hasFullDataSet() return
   *   FALSE. This is useful for e.g. keeping 'rolling timestamp' functionality
   *   working even when you want to use a filter on the GetConnector.
   * - afas_adv_filter (string):
   *   Filter to use (one 'array level' only), as PHP code. This is purely a UI
   *   setting and will probably be split off later. Don't use this in code.
   * - opt_afas_adv_filter: (boolean):
   *   Enables 'afas_adv_filter' setting in settingsForm.
   *
   * - afas_orderby (string):
   *   'orderbyfieldid' argument to pass to the GetConnector call. Only valid
   *   if using the REST API, and if you can make sure all items are fetched at
   *   once (because otherwise the ordering is needed for other purposes); if
   *   this is set when it should not be, an exception is thrown.
   * - afas_skip (integer):
   *   'skip' parameter argument to pass to the GetConnector call. (The
   *   accompanying required 'take' parameter is taken from the 'fetcher_limit'
   *   setting.) Must not be set if 'fetcher_timestamp_from' is set in context
   *   (because then this is needed for other purposes); if this is set when it
   *   should not be, an exception is thrown.
   *
   * - afas_updated_field (string):
   *   AFAS field containing 'updated' date values.
   *   Used only when 'fetcher_timestamp_from' is set in context (by the parent
   *   class), which signifies that a date filter should be added. This class'
   *   inputFilter() method does that automatically, but it needs to know the
   *   name of the 'updated' field for this. If not set, an exception is thrown.
   * - afas_sortable_id_field (string):
   *   AFAS fieldname in the connector containing supposedly-immutable,
   *   supposedly-unique values which the data can be ordered by. (So we should
   *   hope AFAS' database has an index for it.) This enables us to fetch a full
   *   data set in possibly multiple batches. Note that if afas_updated_field is
   *   specified, it is still heavily recommended to specify this in addition.
   *   If 'afas_sortable_id_field' is not specified but the job is able to fetch
   *   data sets in batches (because of e.g. 'fetcher_timestamp_from' context),
   *   'pkey' is used for the same purpose.
   * - afas_sortable_id_field_type (string):
   *   Specify 'date' if the ID field is a date; see Helper::getDataBatch().
   * - afas_sortable_id_field_not_unique (boolean):
   *   Above field must be immutable (or you risk data consistency problems),
   *   but non-uniqueness can be specified. This still introduces risk; not of
   *   lost data, but of exceptions being thrown while running start() - because
   *   in edge cases, the code cannot handle this. See Helper::getDataBatch().
   *
   * - afas_include_empty_fields (boolean):
   *   For SOAP clients only: whether to include empty fields in AFAS output.
   *   SOAP clients do not do this by default, and when this setting is set to
   *   TRUE, empty fields will contain empty string values. For REST clients,
   *   this does not need to be set (and setting it to FALSE explicitly will
   *   cause an exception to be thrown); empty fields will always be present,
   *   and contain null values.
   *
   * - fetcher_timeout (int): <==== NOT FUNCTIONING AT THE MOMENT.
   *   Response_timeout to set for AfasNusoapClient. (Has no effect on
   *   AfasSoapClient. It doesn't matter, as long as we keep using CURL for it.)
   * - fetcher_tries (int):   <==== NOT FUNCTIONING AT THE MOMENT.
   *   Number of times to try connecting at timeout.
   *
   * There's also a 'context value' which is more or less used as a setting (but
   * it's not a setting because it is meant to be set conditionally by a running
   * job):
   * - fetcher_data_set_is_full_despite_implicit_limit (boolean): should be set
   *   if the data set should be regarded complete (which it is not by default,
   *   unless we can fetch the whole data set in batches). In other words it
   *   influences the return value of hasFullDataSet(), which influences some
   *   jobs' behavior. Note this touches on the implicit limit imposed by AFAS;
   *   if an explicit is set using the 'fetcher_limit' setting, that always
   *   implies a data set is not complete.
   * (And there are context values which are both set and read by this class and
   * should not be touched by other classes: afas_already_fetched /
   * afas_refetch_start_value / afas_last_fetched_records. The former must be
   * emptied before fetching a new data set (e.g. if finish() restarts the job).
   */

  /**
   * The default number of records to fetch (per batch).
   *
   * This is taken if the 'take' parameter for AFAS GetConnectors is not
   * specified (through either 'fetcher_limit' or 'afas_take_per_batch' setting)
   * or if the 'fetcher_limit' setting is larger than this value and multiple-
   * batch fetching is possible. (Which depends on other settings/context.)
   *
   * AFAS SOAP GetConnectors provide no good default for this. (If the parameter
   * is not specified in a call, 0 records will be returned.) For REST
   * GetConnectors the default is apparently 100. But maybe that's because a
   * more general use case for REST clients would be e.g. showing data on
   * screen. For jobs which process items, it's better to set it a lot higher;
   * AFAS can deal with it.
   */
  const TAKE_DEFAULT = 1000;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $settings = array()) {
    parent::__construct($settings);

    // opt_afas_adv_filter is only for showing this option in the UI (does not
    // influence behavior) and also is ignored (even if explicitly set) when
    // the user has no 'access drunkins advanced' permission.
    if (!isset($this->settings['opt_afas_adv_filter'])) {
      $this->settings['opt_afas_adv_filter'] = user_access('access drunkins advanced');
    }
  }

  /// Interface functions

  /**
   * {@inheritdoc}
   */
  public function settingsForm() {
    $form = parent::settingsForm();

    // $can_fetch_in_batches in fetchItems() is dependent on context, so we
    // only know for sure that it will be true in some cases, and we never
    // know for sure it will be false. In other words, we can only trust its
    // value if true.
    $can_fetch_in_batches = !empty($this->settings['afas_sortable_id_field'])
                            || !empty($this->settings['opt_fetcher_timestamp']);

    // Functionality which is hidden from non-advanced users:
    // - advanced filter (because possibly confusing / strange syntax /
    //   potential security hole because it uses eval()),
    // - orderby (because practical use unclear / you need to know field names);
    // - per-batch limit (because practical use unclear / confusing).
    if (user_access('access drunkins advanced')) {
      // There's a slight chance that the fieldset has not been defined yet
      // (creating #weight issues), see DrunkinsFetcher.
      if (!isset($form['selection'])) {
        $form['selection'] = array(
          '#type'   => 'fieldset',
          '#title'  => t('Data selection'),
          '#weight' => 10,
        );
      }
      $form['selection']['afas_adv'] = array(
        '#type'        => 'fieldset',
        '#title'       => t('Advanced'),
        '#collapsible' => TRUE,
        '#weight'      => 12,
      );

      if (!empty($this->settings['opt_afas_adv_filter'])) {
        $form['selection']['afas_adv']['afas_adv_filter'] = array(
          '#type'          => 'textarea',
          '#rows'          => 5,
          '#title'         => t('Advanced filter'),
          '#description'   => "Fill one or more (comma separated) array definitions to apply more filters. (This format is a slightly simplified version of the argument to Connection::getData().)<br>
One example, to get only items that changed in the past week:<br><em>array(<br>
'A-DATE-FIELD' => '" . date('Y-m-d\TH:i:s', time() - 7 * 86400) . "',<br>
'#op' => \\PracticalAfas\\Connection::OP_LARGER_OR_EQUAL,<br>
),</em>",
          '#default_value' => "array(
),",
          '#weight'        => 12,
          '#states' => array('enabled' => array(':input[name="cache_items"]' => array('!value' => '2'))),
        );
      }

      $form['selection']['afas_adv']['afas_orderby'] = array(
        '#type'          => 'textfield',
        '#title'         => t('OrderBy parameter'),
        '#description'   => t('The literal parameter as passed to a GetConnector call, i.e. (comma separated) fieldname(s) which may be preceded by hyphen(s).'),
        '#weight'        => 14,
        '#default_value' => !empty($this->settings['afas_orderby']) ? $this->settings['afas_orderby'] : NULL,
        '#states' => array('enabled' => array(':input[name="cache_items"]' => array('!value' => '2'))),
      );
      if ($can_fetch_in_batches) {
        $form['selection']['afas_adv']['afas_orderby']['#description'] .= ' ' . t('Please note: this argument is illegal unless the number of items returned gets limited (to be sure we can fetch all items in one batch).');
      }

      // This should not be explicitly set if we cannot fetch in batches,
      // because in that case an exception will be thrown if it's smaller than
      // 'fetcher_limit'. (It can be set but will be ignored if larger). That's
      // why we mention the default in the description instead of setting it.
      $form['selection']['afas_adv']['afas_take_per_batch'] = array(
        '#type'          => 'textfield',
        '#title'         => t('Per-batch take parameter'),
        '#description'   => t("How many items to fetch in one batch. If not provided, this defaults to @default (except if we can't actually fetch multiple batches).", array('@default' => static::TAKE_DEFAULT)),
        '#weight'        => 16,
        '#default_value' => !empty($this->settings['afas_take_per_batch']) ? $this->settings['afas_take_per_batch'] : NULL,
        '#states' => array('enabled' => array(':input[name="cache_items"]' => array('!value' => '2'))),
      );
    }

    // Checking the textfield is equivalent to checking the 'opt_limit' setting.
    // We are counting on the default behavior of this setting being turned on.
    if (empty($form['selection']['fetcher_limit'])) {
      if (!$can_fetch_in_batches) {
        drupal_set_message("The 'limit' selector is gone from this screen; this will likely mean that this job will throw an exception when starting!", 'warning');
      }
    }
    else {
      if (!$can_fetch_in_batches) {
        // If this is explicitly set to zero, in the end our Client object will
        // throw an exception during start(). We can leave this value unset only
        // if we can fetch in batches. This is probably not the case but we're
        // sure; see comment at $can_fetch_in_batches above.
        $form['selection']['fetcher_limit']['#description'] .= ' ' . t('Please note: a non-zero value is mandated by AFAS, except if the job can fetch items in multiple batches.');
      }
      $form['selection']['afas_skip'] = array(
        '#type'        => 'textfield',
        '#title'       => t('Skip items'),
        '#description' => t('Skip a number of items (before applying above "limit" and/or taking all other items).'),
        '#weight'      => 5,
        '#default_value' => !empty($this->settings['afas_skip']) ? $this->settings['afas_skip'] : NULL,
      );
      if (!isset($this->settings['pkey'])) {
        // We want the user to use skip/take instead of effectively using the
        // same functionality on our side. (But not if the 'single_item' refers
        // to an actual ID value.)
        unset($form['selection']['fetcher_single_item']);
      }
    }

//    $form['fetcher_tries'] = array(
//      '#type'          => 'textfield',
//      '#title'         => t('Tries'),
//      '#description'   => 'The number of times to repeatedly try fetching AFAS data, in case of timeouts',
//      '#default_value' => 1,
//      '#size'          => 3,
//      '#weight'        => 97,
//    );

    return $form;
  }

  /// Extra methods called by parent class / to call by subclasses.

  /**
   * {@inheritdoc}
   */
  public function checkCache() {
    $items = parent::checkCache();
    if ($items) {
      // Implement our 'skip' function. Notes (which should be obvious):
      // - this works because checkCache() always returns all items at once and
      //   does not loop;
      // - if you've fetched items using a 'skip' setting, that does not mean
      //   the setting should be applied again. To get the same items from the
      //   cache later, set skip=0.
      if (!empty($this->settings['afas_skip'])) {
        if (!is_numeric($this->settings['afas_skip'])) {
          throw new RuntimeException(t("Invalid 'afas_skip' setting passed to inputFilter(): %p", array('%p' => drupal_json_encode($this->settings['afas_skip']))));
        }
        $items = array_slice($items, $this->settings['afas_skip']);
      }
    }

    return $items;
  }

  /**
   * {@inheritdoc}
   */
  public function hasFullDataSet(array &$context, $ignore_timestamp = FALSE) {
    // Copy the code from the parent here, but add afas_filter and afas_skip,
    // plus add a check on whether we need to care about the limit to the data
    // set that is always imposed on AFAS GetConnector calls.
    $full = empty($this->settings['afas_filter'])
            && empty($this->settings['afas_skip'])
            && empty($this->settings['fetcher_limit'])
            && empty($this->settings['fetcher_single_item'])
            && !$this->usesCachedItems();
    if ($full) {
      // IF this job is set up to use 'rolling timestamps' (equivalent to:
      // 'opt_fetcher_timestamp' is nonempty, OR the 'fetcher_timestamp_from'
      // context value is nonempty), THEN we have a full data set if no
      // timestamp is set (equivalent to: getStartTimestamp() returns 0 and the
      // context value is empty). We don't need to check the other stuff.
      // IF this job can't use 'rolling timestamps', THEN we have a full data
      // set if we can fetch in batches - OR we return 'full' if we are told
      // not to care about the explicit limit.
      if (!empty($this->settings['opt_fetcher_timestamp']) || !empty($context['fetcher_timestamp_from'])) {
        try {
          $full = $ignore_timestamp ||
                 (empty($context['fetcher_timestamp_from']) && $this->getStartTimestamp() == 0);
        }
        catch (RuntimeException $e) {
          // This means no timestamp was set. That still means the data set is not
          // restricted (even though it won't be able to run). So return 'full'.
        }
      }
      else {
        $full = !empty($this->settings['afas_sortable_id_field'])
                || !empty($context['fetcher_data_set_is_full_despite_implicit_limit']);
      }
    }

    return $full;
  }

  /**
   * {@inheritdoc}
   */
  protected function fetchItems(array &$context) {
    // About rolling timestamp functionality:
    // - To keep checks simple/clear: we only check 'fetcher_timestamp_from'
    //   context, not getStartTimestamp() and the timestamp related job settings
    //   because parent::start() is supposed to have set the context already.
    // - If the context value is set, that means we are using it, we should set
    //   a filter and the 'afas_updated_field' setting is mandatory.
    // - 'afas_updated_field' is always considered mandatory if timestamp
    //   functionality option is enabled, regardless whether it is also ignored
    //   and/or we have a context value for the timestamp. It may not be
    //   strictly mandatory during this job run (IF 'afas_sortable_id_field' is
    //   defined), but it will be during the next run. So better check now.
    $could_use_timestamp = !empty($context['fetcher_timestamp_from']) || !empty($this->settings['opt_fetcher_timestamp']);
    if ($could_use_timestamp && empty($this->settings['afas_updated_field'])) {
      throw new RuntimeException(t('Updated-time field is not specified for AFAS connector, even though we are supposed to be filtering on it (now or in the future).'));
    }

    if (!isset($this->settings['afas_connector'])) {
        throw new RuntimeException("'afas_connector' setting not provided.", 32);
    }
    if (empty($this->settings['afas_connector']) || !is_string($this->settings['afas_connector'])) {
        throw new RuntimeException("Invalid 'afas_connector' setting: " . json_encode($this->settings['afas_connector']), 32);
    }

    $filters = $this->inputFilter($context);

    // Fetching full data set in batches is supported if the data set be ordered
    // by an immutable, hopefully-unique field; see Helper::getDataBatch(). We
    // also accept the 'updated' date for rolling timestamps but it's much
    // better to specify an extra ID field in afas_sortable_id_field.
    // + getStartTimestamp(), to see whether rolling timestamps can be used;
    // parent::start() is supposed to have set the context value already.
    $can_fetch_in_batches = !empty($this->settings['afas_sortable_id_field']) || $could_use_timestamp;
    // That it's supported doesn't mean that we will do it though. Here's the
    // logic that says we know beforehand that we will fetch all items at once:
    $take_per_batch = !empty($this->settings['afas_take_per_batch']) ? $this->settings['afas_take_per_batch']
      : ($can_fetch_in_batches ? static::TAKE_DEFAULT : 0);
    $will_fetch_only_once = !$can_fetch_in_batches
                            || (!empty($this->settings['fetcher_single_item']) && !empty($this->settings['pkey']))
                            || (!empty($this->settings['fetcher_limit'])
                                && (!$take_per_batch || $this->settings['fetcher_limit'] <= $take_per_batch));
    // $will_fetch_only_once implies:
    // - a fetcher_limit is required;
    // - afas_take_per_batch, if specified, cannot be smaller than fetcher_limit
    if (!$will_fetch_only_once) {
      // We need the 'order-by' field for the id field. We won't silently ignore
      // 'afas_orderby' but throw an exception if it's set.
      if (!empty($this->settings['afas_orderby'])) {
        throw new RuntimeException(t("'@setting' setting cannot be set when fetching items in batches / using incremental timestamps.", array('@setting' => 'afas_orderby')));
      }

      $arguments = array(
        'connection' => new Connection($this->getClient()),
        'connector' => $this->settings['afas_connector'],
        'filters' => $filters,
        // Always set 'take'because we don't like the Client default of 100 (and
        // if 'fetcher_limit' is set, Helper::getDataBatch() requires it).
        'take' => $take_per_batch
      );
      if (!empty($this->settings['fetcher_limit'])) {
        $arguments['take_total'] = $this->settings['fetcher_limit'];
      }
      // We can always set 'skip'; it will only be used for the first batch.
      if (!empty($this->settings['afas_skip'])) {
        $arguments['skip'] = $this->settings['afas_skip'];
      }

      if (isset($this->settings['afas_sortable_id_field'])) {
        $arguments['id_field'] = $this->settings['afas_sortable_id_field'];
        if (isset($this->settings['afas_sortable_id_field_type'])) {
          $arguments['id_field_type'] = $this->settings['afas_sortable_id_field_type'];
        }
      }
      elseif (isset($this->settings['pkey'])) {
        $arguments['id_field'] = $this->settings['pkey'];
        // It would be very strange if the primary key field was a date, but:
        if (isset($this->settings['afas_sortable_id_field_type'])) {
          $arguments['id_field_type'] = $this->settings['afas_sortable_id_field_type'];
        }
      }
      elseif ($could_use_timestamp) {
        $arguments['id_field'] = $this->settings['afas_updated_field'];
        $arguments['id_field_type'] = 'date';
        $arguments['id_field_not_unique'] = TRUE;
      }
      if (!empty($this->settings['afas_sortable_id_field_not_unique'])) {
        $arguments['id_field_not_unique'] = $this->settings['afas_sortable_id_field_not_unique'];
      }
      if (!empty($this->settings['afas_include_empty_fields'])) {
        $arguments['options']['Outputoptions'] = Connection::GET_OUTPUTOPTIONS_XML_INCLUDE_EMPTY;
      }

      // Prepare context for getDataBatch(). The properties don't have an 'afas'
      // prefix as we would prefer, but we'll leave it that way.
      if (empty($context['drunkins_rerun_start'])) {
        // Initialize run for fist batch.
        $context['subtotal'] = 0;
      }

      // Apply extra filter for 'updated' field for timestamp-enabled processes,
      // except on repeat batches when the 'updated' field is actually the id
      // field - because in that case, getDataBatch() sets it. (We use
      // subtotal' for checking whether this is the first batch, not
      // 'drunkins_rerun_start'.)
      if (!empty($context['fetcher_timestamp_from']) &&
          (empty($context['subtotal']) || $arguments['id_field'] !== $this->settings['afas_updated_field'])) {
        $arguments['filters'][] = array(
          // We have to set a date without timezone info, expressed in our own
          // timezone, not UTC.
          $this->settings['afas_updated_field'] => date('Y-m-d\TH:i:s', $context['fetcher_timestamp_from']),
          '#op' => Connection::OP_LARGER_OR_EQUAL,
        );
      }

      // Hardcoded reference to Helper class; should be changed if someone ever
      // wants to replace this by a call into a child class.
      $items = \PracticalAfas\Helper::getDataBatch($arguments, $context);

      $context['drunkins_process_more'] = !$context['finished'];
    }
    else {
      // Recap:
      // - We know beforehand that we'll only do one call to AFAS.
      // - We could also end up here if $can_fetch_in_batches is true (though
      //   it's not likely), if an explicit 'small' fetcher_limit is specified.
      //   - In this case, unlike the above code block we don't call yet another
      //     class and we do allow setting an order-by field. (It's debatable
      //     whether the usefulness of this offsets the extra code we'll need to
      //     support this, but here we are.)
      $arguments = array();
      if (!empty($this->settings['afas_skip'])) {
        $arguments['skip'] = $this->settings['afas_skip'];
      }
      if (!empty($this->settings['fetcher_single_item'])) {
        // We should fill something for the sake of SOAP clients, but the value
        // does not really matter since we assume we'll get one item returned.
        $arguments['take'] = $take_per_batch;
      }
      else {
        // We will not allow the output to be silently truncated so the process
        // must specify an explicit limit. (In 'fetcher_limit', because that is
        // the 'real' limit setting; not in 'afas_take_per_batch'.) If we ever
        // encounter a situation where we don't want to specify a limit yet
        // don't care if the output is truncated, we should change this code.
        if (empty($this->settings['fetcher_limit'])) {
          throw new RuntimeException(t("'@setting' setting is required (when fetching items in a single batch).", array('@setting' => 'fetcher_limit')));
        }
        if (!empty($this->settings['afas_take_per_batch']) && $this->settings['afas_take_per_batch'] < $this->settings['fetcher_limit']) {
          throw new RuntimeException(t("'afas_take_per_batch' cannot be smaller than 'fetcher_limit' when fetching items in a single batch."));
        }
        $arguments['take'] = $this->settings['fetcher_limit'];

        if (!empty($this->settings['afas_orderby'])) {
          $arguments['orderbyfieldids'] = $this->settings['afas_orderby'];
        }
        elseif ($can_fetch_in_batches) {
          // For consistency's sake, we still want to set an order-by even if
          // it is not specified. Otherwise e.g. a timestamp-enabled job would
          // be unsorted or sorted, depending on whether we specify a small or
          // large/no fetcher_limit. (Another choice of debatable use...)
          if (!empty($this->settings['afas_sortable_id_field'])) {
            $arguments['orderbyfieldids'] = $this->settings['afas_sortable_id_field'];
          }
          elseif (!empty($this->settings['pkey'])) {
            $arguments['orderbyfieldids'] = $this->settings['pkey'];
          }
          elseif (!empty($this->settings['afas_updated_field'])) {
            $arguments['orderbyfieldids'] = $this->settings['afas_updated_field'];
          }
        }
      }

      // Setting afas_include_empty_fields to FALSE will break for REST clients
      // but we always set it; it's up to the application to not use this setting
      // in 'illegal' cases.
      if (!empty($this->settings['afas_include_empty_fields'])) {
        $arguments['options']['Outputoptions'] = Connection::GET_OUTPUTOPTIONS_XML_INCLUDE_EMPTY;
      }

      // Apply extra filter for 'updated' field; see above. It's very unlikely
      // that a timestamp enabled job will execute here, but possible. (Also see
      // above.)
      if (!empty($context['fetcher_timestamp_from'])) {
        $filters[] = array(
          $this->settings['afas_updated_field'] => date('Y-m-d\TH:i:s', $context['fetcher_timestamp_from']),
          '#op' => Connection::OP_LARGER_OR_EQUAL,
        );
      }

    // @todo while the retry-on-timeout was very necessary in 2012, we shouldn't
    // be doing retry-loops like this inside an HTTP request. We have
    // 'drunkins_process_more' since 2015 so we should set that instead. On top
    // of that, getData() throws exceptions instead of returning FALSE now. So
    // we'll just comment things out for now. If we start seeing timeouts again
    // that are so many they're unmanageable / just failing the job won't fly...
    // then we'll need to test handling them at that moment.
//    $items = FALSE;
//    $tries_left = !empty($this->settings['fetcher_tries']) ? $this->settings['fetcher_tries'] : 1;
//    while ($items === FALSE && --$tries_left >= 0) {

      $connection = new Connection($this->getClient());
      $items = $connection->getData($this->settings['afas_connector'], $filters, Connection::DATA_TYPE_GET, $arguments);

//      if ($items === FALSE) {
//        // Retry if timeout
//        $error = $connection->getLastCallInfo('error');
//        if (strpos($error, 'cURL ERROR: 28') !== FALSE) {
//          if ($tries_left) {
//            // Do 'error' only because it positions this message in between
//            // the other messages, if this is called from the batch form.
//            $this->log('AFAS timeout occurred; retrying, #@try', array('@try' => $this->settings['fetcher_tries'] - $tries_left), WATCHDOG_ERROR);
//          }
//        }
//        else {
//          $tries_left = 0;
//        }
//      }
//    }
    }

    return $items;
  }

  /**
   * Returns the filter to use for the AFAS call.
   *
   * A 'rolling timestamp' related filter is not added by this method; that is
   * too much tied to the code in fetchItems(), so it's added in there.
   *
   * @param array $context
   *   The process context array. Unused at the moment.
   *
   * @return array
   *   'filters array' that can be used for calling Connection::getData().
   *
   * @throws \RuntimeException
   *   If the filter or related settings are invalid. Many setting/context
   *   values may be required depending on the presence of others.
   */
  public function inputFilter(array $context) {
    // This supports 'simple' (array key being the field name) and 'deeper'
    // array arguments; see \PracticalAfas\Connection::parseFilters().
    $filters = isset($this->settings['afas_filter']) ? $this->settings['afas_filter'] : array();
    if (!is_array($filters)) {
      throw new RuntimeException(t("'@setting' setting must be an array.", array('@setting' => 'afas_filter')));
    }

    // The below options support only 'simple' filter settings because they are
    // added 1 layer deeper. Connection::getData() will sort it out.

    // 'single item' filter: can be added if 'pkey' is set. Ignore '' at least
    // for as long as we have a UI built in.
    if (isset($this->settings['fetcher_single_item']) && $this->settings['fetcher_single_item'] !== '') {
      // Let's warn if 'pkey' is not set. (In that case, the UI did not expose
      // 'fetcher_single_item'.)
      if (empty($this->settings['pkey'])) {
        $this->log("'fetcher_single_item' setting is provided but 'pkey' is not. This means the query will not be automatically filtered by the 'fetcher_single_item' value.", array(), WATCHDOG_WARNING);
      }
      else {
        $filters[] = array($this->settings['pkey'] => $this->settings['fetcher_single_item']);
      }
    }

    // General filter which does not impact 'completeness'.
    if (isset($this->settings['afas_full_data_set_filter'])) {
      if (!is_array($this->settings['afas_full_data_set_filter'])) {
        throw new RuntimeException(t("'@setting' setting must be an array.", array('@setting' => 'afas_full_data_set_filter')));
      }
      $filters[] = $this->settings['afas_full_data_set_filter'];
    }

    // Advanced filter.
    $adv = $this->evalFilterInput();
    if ($adv) {
      $filters[] = $adv;
    }

    return $filters;
  }

  /// Protected helper functions

  /**
   * Returns an initialized AFAS client object.
   *
   * @return object
   */
  protected function getClient() {
    $client_class = !empty($this->settings['afas_client_class']) ? $this->settings['afas_client_class']
      : variable_get('afas_api_client_class', 'DrupalSoapAppClient');
    $client_options = isset($this->settings['afas_client_options']) ? $this->settings['afas_client_options'] : array();
    if (!is_array($client_options)) {
      throw new RuntimeException(t("'@setting' setting is not an array.", array('@setting' => 'afas_client_options')));
    }
    if (!empty($this->settings['fetcher_timeout']) && is_numeric($this->settings['fetcher_timeout'])) {
      // Option only works for AfasNusoapClient but that's fine.
      $client_options['response_timeout'] = $this->settings['fetcher_timeout'];
    }
    return new $client_class($client_options);
  }

  /**
   * Checks for/evaluates a comma separated list of arrays in
   * $this->settings['afas_adv_filter']. This is used for the 'Filter' textarea
   * value in the batch form.
   *
   * @return array
   *   The evaluated filters, or empty array if nothing set / error encountered.
   *
   * @throws \RuntimeException
   *   If this filter does not evaluate to valid PHP array structures.
   */
  protected function evalFilterInput() {
    static $filters;

    if (!isset($filters)) {
      $filters = array();

      if (!empty($this->settings['afas_adv_filter'])) {
        $eval = '';
        // Can cause uncatchable (at least in PHP5?) fatal error. As stated on
        // top of this class: don't use this in code; it's a pure UI option that
        // will probably be split off later.
        eval('$eval = array(' . $this->settings['afas_adv_filter'] . ');');
        if (is_array($eval)) {
          foreach ($eval as $filter) {
            if (is_array($filter)) {
              if (!empty($filter)) {
                $filters[] = $filter;
              }
              // Just disregard filter-parts which are empty arrays.
            }
            else {
              // t() in Exception?
              throw new RuntimeException(t("Invalid 'filter' (part) setting passed to inputFilter(): %p", array('%p' => var_export($filter, TRUE))));
            }
          }
        }
        else {
          throw new RuntimeException(t("Invalid 'afas_adv_filter' setting passed to inputFilter(): %p", array('%p' => var_export($this->settings['afas_adv_filter'], TRUE))));
        }
      }
    }
    return $filters;
  }

}
